package dc.ac.ppad.beans;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;

/**
 * Session Bean implementation class MyStatelessSessionBean
 */
@Stateless
@LocalBean
public class MyStatelessSessionBean implements MyStatelessSessionRemote {

	/**
	 * Default constructor.
	 */
	public MyStatelessSessionBean() {

		System.out.println("Constructor called: " + MyStatelessSessionBean.class.getName());
	}

	@Override
	public String runEcho(String string) {

		System.out.println("runEcho called...");
		return "Echo: " + string;
	}

}
